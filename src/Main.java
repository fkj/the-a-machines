import java.io.IOException;

import machine.Interpreter;
import machine.ObjectCode;
import machine.Parser;
import machine.Tokenizer;
import test.TestDriver;

public class Main {
  //private static String path = "./programs/test-v5-1.asm";
  //private static String path = "./programs/test-v5-2.asm";
  private static String path = "./programs/test-v5-3.asm";
  
  public static void main(String[] args) {
    runTests();
  }
  
  private static void runTests() {
    TestDriver runner = new TestDriver("./tests/tests.txt");
    runner.run();
    runner.close();
  }
  
  private static void testInterpreter() {
    Interpreter interpreter = new Interpreter(path);
    interpreter.compile();
    interpreter.run();
  }
  
  private static void testParser() {
    Parser p = new Parser(path);
    
    ObjectCode obj = p.Parse();

    System.out.println(obj);
  }
  
  private static void testTokenizer() {
    try {
      Tokenizer t = new Tokenizer(path);
      
      while (t.hasMore())
        System.out.println("[" + t.next() + "]");
      
      t.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}

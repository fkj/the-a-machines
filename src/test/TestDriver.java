package test;

import java.io.*;

import machine.Interpreter;
import machine.State;

public class TestDriver {
  private String path;
  private BufferedReader reader;
  
  // pseudo parameters
  private int testNo;
  private String testTitle;

  public TestDriver(String path) {
    try {
      this.path = path;
      
      reader = new BufferedReader(new FileReader(path));
    }
    catch (IOException e) {
      System.out.println("Failed to open: " + path);
    }
  }
  
  public void run() {
    try {
      int failedTests = 0;
      boolean failed;

      testNo = 0;
      String line = readLine();
      
      while (line != null) {
        if (headEquals(line, "Test")) {
          testNo++;
          failed = false;
          testTitle = tail(line);
          
          /*
           * read program (and write it to temp file)
           */
          File tempFile = File.createTempFile("test-", ".asm");
          // e.g.: "C:\Users\fkj\AppData\Local\Temp\test-9034716908543053256.asm"
          
          try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {
            while ((line = readLine()) != null &&
                !headEquals(line, "Assert") &&
                !headEquals(line, "Test")) {
              writer.write(line);
              writer.newLine();
            }
          }
          
          boolean expectedToFail = headEquals(line, "Assert") &&
                                   tailEquals(line, "fail");
          
          /*
           * run program
           */
          State state = null;
          
          try {
            String tempPath = tempFile.getAbsolutePath();
            Interpreter interpreter = new Interpreter(tempPath);
            interpreter.compile();
            interpreter.setShowTrace(false);
            state = interpreter.run();
            
            if (expectedToFail) {
              println("Did not fail!");
              
              failed = true;
            }
          }
          catch (Exception e) {
            if (!expectedToFail) {
              println("Failed to compile/execute program");
              println(e);
              
              failed = true;
            }
          }
          
          // cleanup
          tempFile.delete();
          
          /*
           * check assertions
           */
          if (headEquals(line, "Assert"))
            while ((line = readLine()) != null && !headEquals(line, "Test"))
              if (state != null)
                failed |= !assertState(state, line);
          
          if (failed)
            failedTests++;
          
        } else {
          println("Expected next Test Header, but found: " + line);
          break;
        }
      }
      
      int succeded = testNo - failedTests;
      System.out.println(succeded + " of " + testNo + " tests succeded.");
    }
    catch (IOException e) {
      System.out.println("I/O Error reading file: " + path);
    }
  }
  
  private boolean assertState(State state, String assertion) {
    String head = head(assertion).toUpperCase();
    String tail = tail(assertion);

    switch (head) {
      case "AC":
      case "X":
      case "Y":
      case "PC":
      case "SP": {
        int expected = Integer.parseInt(tail);
        int observed = state.getRegisterValueByName(head);
        
        return assertEquals(head, expected, observed);
      }
      
      default: {
        if (head.length() == 2) {
          // flag test
          String flagName = head.substring(0, 1);
          boolean expected = (head.charAt(1) == '+' ? true : false);
          boolean observed = state.getFlagValueByName(flagName);
          
          return assertFlag(flagName, expected, observed);
          
        } else if (head.startsWith("MEM[") && head.endsWith("]")) {
          // memory check
          String addressStr = head.substring(4, head.length() - 1);
          int address = Integer.parseInt(addressStr);
          
          int expected = Integer.parseInt(tail);
          int observed = state.getMem(address);

          return assertEquals("mem[" + address + "]", expected, observed);
          
        } else {
          println("Could not understand assertion: " + assertion);
          return false;
        }
      }
    }
  }
  
  private boolean assertFlag(String flagName, boolean expected, boolean observed) {
    if (observed != expected) {
      println("(flag: " + flagName + 
          ") expected: " + flagState(expected) + 
          ", observed: " + flagState(observed));
      return false;
    } else
      return true;
  }
  
  private String flagState(boolean value) {
    return value ? "set" : "cleared";
  }
  
  private boolean assertEquals(String errorMessage, Object expected, Object observed) {
    if (observed != expected) {
      println(errorMessage +
          ": expected: " + expected + 
          ", observed: " + observed);
      return false;
    } else
      return true;
  }
  
  private void println(Exception e) {
    StringWriter errors = new StringWriter();
    e.printStackTrace(new PrintWriter(errors));
    println(errors.toString());
  }
  
  private void println(String message) {
    System.out.println("[" + testNo + "] \"" + testTitle + "\": " + message);
  }
  
  private boolean headEquals(String text, String target) {
    return (text != null && head(text).equals(target));
  }
  
  private String head(String line) {
    int index = line.indexOf(":");
    
    if (index >= 0)
      return line.substring(0, index).trim();
    else
      return line.trim();
  }
  
  private boolean tailEquals(String text, String target) {
    return (text != null && tail(text).equals(target));
  }
  
  private String tail(String line) {
    int index = line.indexOf(":");
    
    if (index >= 0)
      return line.substring(index + 1).trim();
    else
      return line.trim();
  }
  
  private String readLine() throws IOException {
    while (reader.ready()) {
      String line = reader.readLine().trim();
      
      if (line.length() > 0 && !line.startsWith("#"))
        return line;
    }
    
    return null; // no more lines (EOF)
  }
  
  public void close() {
    try {
      reader.close();
    }
    catch (IOException e) {
      // ignore
    }
  }
}

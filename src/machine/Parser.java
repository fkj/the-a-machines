package machine;

import java.io.IOException;

import machine.instructions.*;
import machine.instructions.arguments.*;

public class Parser {
  private String path;
  
  public Parser(String path) {
    this.path = path;
  }

  public ObjectCode Parse() {
    ObjectCode obj = new ObjectCode();
    
    try {
      Tokenizer tokenizer = new Tokenizer(path);
      
      while (tokenizer.hasMore()) {
        String token = tokenizer.next();
        
        if (InstBase.isInstName(token)) {
          // instruction
          Inst inst = InstBase.createInstByName(token);
          
          if (InstBase.hasArg(token)) {
            Arg arg = parseArgument(tokenizer);
            
            if (arg instanceof ImmediateArg && !InstBase.acceptsImmediate(token))
              throw new RuntimeException("Instruction does not accept Immediate: " + inst);
            
            inst.setArgument(arg);
          }
    
          obj.addInst(inst);
          
        } else if (tokenizer.nextEquals(":"))
          // label
          obj.addLabel(token);
        else
          throw new RuntimeException("Expecting ':' after label: " + token);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
      System.exit(0);
    }
    
    return obj;
  }
  
  private Arg parseArgument(Tokenizer tokenizer) throws IOException {
    String token = tokenizer.next();
    
    if (token.equals("#"))
      return new ImmediateArg(parseInteger(tokenizer));
    
    else if (token.equals("(")) {
      // register indirect/indexed
      String register = tokenizer.next();
      
      token = tokenizer.next();
      
      if (token.equals(")"))
        // register indirect
        return new RegisterIndirectArg(register);
      
      else if (token.equals(",")) {
        // register indexed
        if (tokenizer.nextEquals("#")) {
          int offset = parseInteger(tokenizer);
          
          if (!tokenizer.nextEquals(")"))
            throw new RuntimeException("Register Indexed: expecting ')'");
          
          return new RegisterIndexedArg(register, offset);
          
        } else
          throw new RuntimeException("Register Indexed: expecting '#'");
        
      } else // error
        throw new RuntimeException("Register Indirect/Indexed: expecting ')' or ','");
      
      
    } else if (isInteger(token)) {
      // address
      int address = Integer.parseInt(token);
      
      return new DirectAddressingArg(address);
      
    } else
      // label
      return new DirectAddressingArg(token);
  }
  
  private boolean isInteger(String s) {
    for (char c : s.toCharArray())
      if (!Character.isDigit(c))
        return false;
    
    return true;
  }
  
  private int parseInteger(Tokenizer tokenizer) throws IOException {
    String intLiteral = tokenizer.next();
    
    if (intLiteral.equals("-"))
      intLiteral += tokenizer.next();
    
    return Integer.parseInt(intLiteral);
  }
}

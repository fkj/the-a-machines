package machine;

public class State {
  private int ac; // accumulator
  private int x, y; // x and y registers
  private boolean negativeFlag, zeroFlag;
  private int pc; // program counter
  private int[] mem; // memory
  private int sp; // stack pointer
  
  public State(int size) {
    ac = 0;
    x = y = 0;
    
    negativeFlag = false;
    zeroFlag = true;
    
    pc = 0;
    
    mem = new int[size];
    
    sp = mem.length - 1; // top-down
  }

  public int getAC() {
    return ac;
  }

  public void setAC(int ac) {
    this.ac = ac;
    
    updateFlags(this.ac);
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
    
    updateFlags(this.x);
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
    
    updateFlags(this.y);
  }

  public int getPC() {
    return pc;
  }

  public void setPC(int pc) {
    this.pc = pc;
  }
  
  public void incPC() {
    this.pc++;
  }
  
  public int getMem(int address) {
    return mem[normalizeAddress(address)];
  }
  
  public void setMem(int address, int value) {
    mem[normalizeAddress(address)] = value;
  }
  
  private int normalizeAddress(int address) {
    // if underflow
    while (address < 0)
      address += mem.length;
    
    // if overflow
    return address % mem.length;
  }
  
  public int getRegisterValueByName(String register) {
    switch (register.toUpperCase()) {
      case "AC":
        return ac;
        
      case "X":
        return x;
        
      case "Y":
        return y;
        
      case "PC":
        return pc;
        
      case "SP":
        return sp;
      
      default:
        throw new RuntimeException("Unknown register: " + register);
    }
  }
  
  public boolean getFlagValueByName(String flagName) {
    switch (flagName.toUpperCase()) {
      case "Z":
        return zeroFlag;
        
      case "N":
        return negativeFlag;
      
      default:
        throw new RuntimeException("Unknown flag: " + flagName);
    }
  }
  
  public void push(int value) {
    mem[sp] = value;
    
    if (sp > 0)
      sp--;
    else
      // wrap around
      sp = mem.length - 1;
  }
  
  public int pop() {
    if (sp < mem.length - 1)
      sp++;
    else
      // wrap around
      sp = 0;
    
    return mem[sp];
  }
  
  public void updateFlags(int value) {
    negativeFlag = (value < 0);
    zeroFlag = (value == 0);
  }
  
  public boolean isFlagNegative() {
    return negativeFlag;
  }
  
  public boolean isFlagZero() {
    return zeroFlag;
  }
  
  @Override
  public String toString() {
    return
        "AC:" + ac + ", " +
        "X:" + x + ", " +
        "Y:" + y + ", " +
        "PC:" + pc + ", " +
        "SP:" + sp + ", " +
        "N" + (negativeFlag ? "+" : "-") +
        "Z" + (zeroFlag ? "+" : "-");
  }
}

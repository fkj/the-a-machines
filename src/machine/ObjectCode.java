package machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import machine.instructions.Inst;

public class ObjectCode {
  private ArrayList<Inst> insts;
  private Map<String, Integer> labels; 

  public ObjectCode() {
    insts = new ArrayList<Inst>();
    labels = new HashMap<String, Integer>(); 
  }
  
  public void addInst(Inst inst) {
    insts.add(inst);
  }
  
  public Inst getInst(int index) {
    return insts.get(index);
  }
  
  public void addLabel(String name) {
    // ...size() will be the index of the next instruction
    labels.put(name, insts.size());
  }
  
  public int getLabelAddress(String label) {
    return labels.get(label).intValue();
  }
  
  public boolean isLegalPC(int pc) {
    return 0 <= pc && pc < insts.size();
  }
  
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();

    if (insts.size() > 0) {
      sb.append("Instructions:\n");
      
      for (int i=0; i<insts.size(); i++)
        sb.append("  " + i + ": " + insts.get(i).toString() + "\n");
    }

    if (labels.size() > 0) {
      sb.append("Labels:\n");

      for (String label : labels.keySet())
        sb.append("  " + label + ": " + labels.get(label));
    }
    
    return sb.toString();
  }
}

// return insts.toArray(new Inst[0]);

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class TransferACtoY extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setY(state.getAC());
  }
}

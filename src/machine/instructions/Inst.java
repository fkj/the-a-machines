package machine.instructions;

import machine.ObjectCode;
import machine.State;
import machine.instructions.arguments.Arg;

public abstract class Inst {
  protected Arg arg;
  
  public Inst() {
    arg = null;
  }
  
  public void setArgument(Arg arg) {
    this.arg = arg;
  }
  
  protected void setPC(State state, int address) {
    state.setPC(address);
  }
  
  public abstract void execute(State state, ObjectCode obj);
  
  @Override
  public String toString() {
    String className = this.getClass().getSimpleName();
    
    String argStr = "";
    
    if (arg != null)
      argStr = arg.toString();
    
    return className + " " + argStr;
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class StoreAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    arg.setValue(state, obj, state.getAC());
  }
}

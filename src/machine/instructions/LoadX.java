package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class LoadX extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setX(arg.getValue(state, obj));
  }
}

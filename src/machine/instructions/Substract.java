package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class Substract extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setAC(state.getAC() - arg.getValue(state, obj));
  }
}

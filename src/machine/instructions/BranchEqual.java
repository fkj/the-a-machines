package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class BranchEqual extends ConditionalJump {

  @Override
  protected boolean condition(State state, ObjectCode obj) {
    return state.isFlagZero();
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class PullAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setAC(state.pop());
  }
}

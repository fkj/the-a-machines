package machine.instructions.arguments;

public abstract class RegisterArg extends Arg {
  protected String register;
  
  public RegisterArg(String register) {
    this.register = register;
  }
  
  @Override
  public String toString() {
    return register.toUpperCase();
  }
}

package machine.instructions.arguments;

import machine.ObjectCode;

public abstract class AddressingArg extends Arg {
  private int address;
  
  public AddressingArg(int address) {
    this.address = address;
  }

  public AddressingArg(String label) {
    super(label);
    
    this.address = -1;
  }
  
  public int getAddress(ObjectCode obj) {
    if (this.address == -1)
      // resolve address (only happens first time)
      this.address = obj.getLabelAddress(this.label);

    return address;
  }
  
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    
    if (this.address == -1)
      sb.append("not resolved");
    else
      sb.append("" + address);
    
    return sb.toString();
  }
}

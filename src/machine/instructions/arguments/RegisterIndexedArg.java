package machine.instructions.arguments;

import machine.ObjectCode;
import machine.State;

public class RegisterIndexedArg extends RegisterArg {
  private int offset;
  
  public RegisterIndexedArg(String register, int offset) {
    super(register);
    
    this.offset = offset;
  }

  @Override
  public int getValue(State state, ObjectCode obj) {
    return state.getMem(state.getRegisterValueByName(register) + offset);
  }

  @Override
  public void setValue(State state, ObjectCode obj, int value) {
    state.setMem(state.getRegisterValueByName(register) + offset, value);
  }
  
  @Override
  public String toString() {
    return "RegisterIndexed: " + super.toString() + "+" + offset;
  }
}

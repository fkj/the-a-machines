package machine.instructions.arguments;

import machine.ObjectCode;
import machine.State;

public abstract class Arg {
  protected String label;
  
  public Arg() {
    this(null);
  }
  
  public Arg(String label) {
    setLabel(label);
  }
  
  public void setLabel(String label) {
    this.label = label;
  }

  public abstract int getValue(State state, ObjectCode obj);
  public abstract void setValue(State state, ObjectCode obj, int value);
}

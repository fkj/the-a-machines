package machine.instructions.arguments;

import machine.ObjectCode;
import machine.State;

public class RegisterIndirectArg extends RegisterArg {
  
  public RegisterIndirectArg(String register) {
    super(register);
  }

  @Override
  public int getValue(State state, ObjectCode obj) {
    return state.getMem(state.getRegisterValueByName(register));
  }

  @Override
  public void setValue(State state, ObjectCode obj, int value) {
    state.setMem(state.getRegisterValueByName(register), value);
  }
  
  @Override
  public String toString() {
    return "RegisterIndirect: " + super.toString();
  }
}

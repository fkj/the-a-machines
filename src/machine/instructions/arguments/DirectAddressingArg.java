package machine.instructions.arguments;

import machine.ObjectCode;
import machine.State;

public class DirectAddressingArg extends AddressingArg {
  
  public DirectAddressingArg(int address) {
    super(address);
  }

  public DirectAddressingArg(String label) {
    super(label);
  }

  @Override
  public int getValue(State state, ObjectCode obj) {
    return state.getMem(getAddress(obj));
  }

  @Override
  public void setValue(State state, ObjectCode obj, int value) {
    state.setMem(getAddress(obj), value);
  }
  
  @Override
  public String toString() {
    return "DirectAddressing: " + super.toString();
  }
}

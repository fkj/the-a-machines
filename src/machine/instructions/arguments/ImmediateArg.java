package machine.instructions.arguments;

import machine.ObjectCode;
import machine.State;

public class ImmediateArg extends Arg {
  private int value;
  
  public ImmediateArg(int value) {
    this.value = value;
  }

  @Override
  public int getValue(State state, ObjectCode obj) {
    return value;
  }

  @Override
  public void setValue(State state, ObjectCode obj, int value) {
    throw new RuntimeException("Cannot store to immediate value");
  }
  
  @Override
  public String toString() {
    return "Immediate " + value;
  }
}

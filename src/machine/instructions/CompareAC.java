package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class CompareAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.updateFlags(state.getAC() - arg.getValue(state, obj));
  }
}

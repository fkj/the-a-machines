package machine.instructions;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.HashMap;

public class InstBase {
  private static Map<String, InstInfo> instToClassName;
  
  static {
    instToClassName = new HashMap<String, InstInfo>();
    instToClassName.put("LDA", new InstInfo("LoadAC", true));
    instToClassName.put("STA", new InstInfo("StoreAC", true, false));
    instToClassName.put("ADD", new InstInfo("Add", true));
    instToClassName.put("SUB", new InstInfo("Substract", true));
    instToClassName.put("IN",  new InstInfo("Input"));
    instToClassName.put("OUT", new InstInfo("Output"));
    instToClassName.put("BRK", new InstInfo("Break"));
    
    instToClassName.put("JMP", new InstInfo("Jump", true));
    instToClassName.put("BEQ", new InstInfo("BranchEqual", true));
    instToClassName.put("BNE", new InstInfo("BranchNotEqual", true));
    instToClassName.put("BPL", new InstInfo("BranchPlus", true));
    instToClassName.put("BMI", new InstInfo("BranchMinus", true));
    
    instToClassName.put("LDX", new InstInfo("LoadX", true));
    instToClassName.put("LDY", new InstInfo("LoadY", true));
    instToClassName.put("STX", new InstInfo("StoreX", true, false));
    instToClassName.put("STY", new InstInfo("StoreY", true, false));
    instToClassName.put("TAX", new InstInfo("TransferACtoX"));
    instToClassName.put("TAY", new InstInfo("TransferACtoY"));
    instToClassName.put("TXA", new InstInfo("TransferXtoAC"));
    instToClassName.put("TYA", new InstInfo("TransferYtoAC"));
    instToClassName.put("DEC", new InstInfo("DecrementAC"));
    instToClassName.put("DEX", new InstInfo("DecrementX"));
    instToClassName.put("DEY", new InstInfo("DecrementY"));
    instToClassName.put("INC", new InstInfo("IncrementAC"));
    instToClassName.put("INX", new InstInfo("IncrementX"));
    instToClassName.put("INY", new InstInfo("IncrementY"));
    
    instToClassName.put("CMP", new InstInfo("CompareAC", true));
    instToClassName.put("CPX", new InstInfo("CompareX", true));
    instToClassName.put("CPY", new InstInfo("CompareY", true));
    
    instToClassName.put("PHA", new InstInfo("PushAC"));
    instToClassName.put("PLA", new InstInfo("PullAC"));
    
    instToClassName.put("JSR", new InstInfo("JumpSubroutine", true));
    instToClassName.put("RTS", new InstInfo("ReturnSubroutine"));
  }
  
  public static boolean isInstName(String instName) {
    return instToClassName
        .containsKey(instName.toUpperCase());
  }
  
  // PRE: instName is legal
  public static boolean hasArg(String instName) {
    return instToClassName
        .get(instName.toUpperCase())
        .hasArg();
  }

  // PRE: instName is legal
  public static boolean acceptsImmediate(String instName) {
    return instToClassName
        .get(instName.toUpperCase())
        .acceptsImmediate();
  }

  // PRE: instName is legal
  public static Inst createInstByName(String instName) {
    try {
      String className = instToClassName
          .get(instName.toUpperCase())
          .getClassName();
      Class cls = Class.forName("machine.instructions." + className);
      Constructor ctr = cls.getConstructor();
      return (Inst) ctr.newInstance();
    }
    catch (Exception e) {
      // there are may possible exception types,
      // but none should be cast (in release version)
      e.printStackTrace();
      return null;
    }
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class CompareY extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.updateFlags(state.getY() - arg.getValue(state, obj));
  }
}

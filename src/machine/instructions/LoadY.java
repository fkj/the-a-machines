package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class LoadY extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setY(arg.getValue(state, obj));
  }
}

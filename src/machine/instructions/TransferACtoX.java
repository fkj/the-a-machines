package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class TransferACtoX extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setX(state.getAC());
  }
}

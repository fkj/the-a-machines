package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class ReturnSubroutine extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    // jump to the return address, stored on the stack
    setPC(state, state.pop());
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class IncrementAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setAC(state.getAC() + 1);
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class LoadAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setAC(arg.getValue(state, obj));
  }
}

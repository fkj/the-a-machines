package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class TransferYtoAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setAC(state.getY());
  }
}

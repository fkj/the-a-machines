package machine.instructions;

public class InstInfo {
  private String className;
  private boolean hasArg;
  private boolean immediate;
  
  public InstInfo(String className) {
    this(className, false, false);
  }
  
  public InstInfo(String className, boolean hasArg) {
    this(className, hasArg, true);
  }
  
  public InstInfo(String className, boolean hasArg, boolean immediate) {
    this.className = className;
    this.hasArg = hasArg;
    this.immediate = immediate;
  }
  
  public String getClassName() {
    return className;
  }
  
  public boolean hasArg() {
    return hasArg;
  }
  
  public boolean acceptsImmediate() {
    return immediate;
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class Output extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    System.out.println(state.getAC());
  }
}

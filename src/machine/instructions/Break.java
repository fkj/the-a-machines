package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class Break extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.setPC(-1 - 1);
  }
}

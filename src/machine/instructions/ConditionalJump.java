package machine.instructions;

import machine.ObjectCode;
import machine.State;

public abstract class ConditionalJump extends Jump {

  @Override
  public void execute(State state, ObjectCode obj) {
    if (condition(state, obj))
      super.execute(state, obj);
  }
  
  protected abstract boolean condition(State state, ObjectCode obj);
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class PushAC extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.push(state.getAC());
  }
}

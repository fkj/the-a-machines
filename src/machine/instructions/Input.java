package machine.instructions;

import java.util.Scanner;

import machine.ObjectCode;
import machine.State;

public class Input extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    while (true) {
      try {
        System.out.print("> ");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        scanner.close();
        System.out.println();
        
        state.setAC(value);
        return;
      }
      catch (Exception e) {
        // try again
        System.out.println("please enter an integer value!");
      }
    }
  }
}

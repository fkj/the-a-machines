package machine.instructions;

import machine.ObjectCode;
import machine.State;
import machine.instructions.arguments.AddressingArg;

public class JumpSubroutine extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    // PC is the address of the next instruction,
    // the one that we should return to execute
    // after the subroutine call.
    
    // store return address on stack
    state.push(state.getPC());
    
    // jump to subroutine
    setPC(state, ((AddressingArg) arg).getAddress(obj));
  }
}

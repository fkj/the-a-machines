package machine.instructions;

import machine.ObjectCode;
import machine.State;
import machine.instructions.arguments.AddressingArg;

public class Jump extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    setPC(state, ((AddressingArg) arg).getAddress(obj));
  }
}

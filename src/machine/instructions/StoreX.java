package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class StoreX extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    arg.setValue(state, obj, state.getX());
  }
}

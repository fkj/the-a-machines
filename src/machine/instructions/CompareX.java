package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class CompareX extends Inst {

  @Override
  public void execute(State state, ObjectCode obj) {
    state.updateFlags(state.getX() - arg.getValue(state, obj));
  }
}

package machine.instructions;

import machine.ObjectCode;
import machine.State;

public class BranchPlus extends ConditionalJump {

  @Override
  protected boolean condition(State state, ObjectCode obj) {
    return !state.isFlagNegative() && !state.isFlagZero();
  }
}

package machine;

import machine.instructions.Inst;

public class Interpreter {
  private String path;
  private ObjectCode obj;
  private boolean showTrace;

  public Interpreter(String path) {
    this.path = path;
    
    this.showTrace = true;
  }
  
  public void setShowTrace(boolean showTrace) {
    this.showTrace = showTrace;
  }

  public void compile() {
    Parser parser = new Parser(path);
    obj = parser.Parse();
  }
  
  public State run() {
    State state = new State(100);
    
    while (obj.isLegalPC(state.getPC())) {
      Inst inst = obj.getInst(state.getPC());
      
      if (showTrace)
        printTrace(state, obj);

      state.incPC(); // inc PC _before_ executing instruction
      
      inst.execute(state, obj);
    }
    
    return state;
  }
  
  private void printTrace(State state, ObjectCode obj) {
    System.out.println(state);
    
    System.out.println(
        state.getPC() + ": " +
        obj.getInst(state.getPC()));
  }
}

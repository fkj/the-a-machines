package machine;

import java.io.FileReader;
import java.io.IOException;

public class Tokenizer {
  private FileReader reader;
  private char pushBack;
  
  private static final char NO_CHAR = '\0';
  
  public Tokenizer() {
    setPushBack(NO_CHAR);
  }

  public Tokenizer(String path) throws IOException {
    reader = new FileReader(path);
  }
  
  public void close() throws IOException {
    reader.close();
  }
  
  private void setPushBack(char c) {
    pushBack = c;
  }
  
  public boolean hasMore() throws IOException {
    if (pushBack != NO_CHAR)
      // we have somthing waiting in puchback
      return true;
    else {
      // look in the file
      char c = skipToNext();
      
      if (c != NO_CHAR) {
        // more token chars
        setPushBack(c);
        return true;
      } else
        // nothing more
        return false;
    }
  }
  
  public boolean nextEquals(String target) throws IOException {
    if (hasMore())
      return next().equals(target);
    else
      return false;
  }

  public String next() throws IOException {
    char c = skipToNext();
    
    if (c != NO_CHAR) {
      StringBuffer sb = new StringBuffer();
      sb.append(c);
      
      if (Character.isLetter(c) || c == '_')
        // as long as it's a letter or digit or underscore, add it
        readSequence(sb, (cc) -> {
          return Character.isLetter(cc) ||
                 Character.isDigit(cc) ||
                 cc == '_';
        });
        
      else if (Character.isDigit(c))
        // as long as it's a digit, add it
        readSequence(sb, (cc) -> {
          return Character.isDigit(cc);
        });
        
      // else => take it one char at a time
      
      return sb.toString();
    } else
      return null;
  }
  
  private interface Acceptor {
    public boolean accept(char c);
  }
  
  private void readSequence(StringBuffer sb, Acceptor acceptor) throws IOException {
    while (true) {
      int i = reader.read();
      
      if (i == -1)
        // EOF
        return;
      
      char cc = (char) i;
      
      if (acceptor.accept(cc))
        sb.append(cc);
      else {
        if (!Character.isWhitespace(cc)) 
          setPushBack(cc);
        break;
      }
    }
  }
  
  private char skipToNext() throws IOException {
    if (pushBack != NO_CHAR) {
      char c = pushBack;
      setPushBack(NO_CHAR);
      return c;
    }
    
    int i;
    
    while ((i = reader.read()) != -1) { // while (not EOF)
      char c = (char) i;
      
      if (c == ';') {
        // end of line comment
        while ((i = reader.read()) != -1) { // while (not EOF)
          c = (char) i;
          
          if (c == '\r' || c == '\n')
            // left whitespace fall through
            break;
        }
        
        if (i == -1) // if (EOF)
          c = ' '; // 'injecting' whitespace in place of EOF
      }
      
      if (!Character.isWhitespace(c))
        return c;
    }
    
    return NO_CHAR;
  }
}

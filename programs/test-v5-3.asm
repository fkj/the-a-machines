;
; demonstrate use of subroutine with JSR and RTS
;

; The use of the local variable 'product' is not
; optimized to make the example more educational

; We will use C-like calling convention
;   caller must preserve AC if needed
;   params are pushed last parameter first
;   then the return address (done by JSR)
;   then local variables
;   return value is returned in AC
 
    jmp main      ; jump to main
    
multiply:         ; int multiply(int a, int b)
    lda #0        ; int product=0;
    pha           ; 'product' is local variable
    ldx (SP,#4)   ; X = parameter b;

; stack layout:
; SP+4: parameter: b
; SP+3: parameter: a
; SP+2: return address
; SP+1: local variable: product

repeat:
    cpx #0        ; if (X == 0) goto done;
    beq done      ;
    lda (SP,#1)   ; AC = product;
    add (SP,#3)   ; AC += parameter a;
    sta (SP,#1)   ; product = AC;
    out           ; print AC; // (i.e. product)
    dex           ; X = X-1; // 'decrement b'
    jmp repeat    ; try again
    
done:
    pla           ; AC = product; // and removing local variable
    rts           ; return

main:             ; program starts here
    lda #8        ; preserving 'old' AC value
    pha           ;
                  ; AC = multiply(a:5, b:4);
    lda #4        ; push b:4
    pha           ;
    lda #5        ; push a:5
    pha           ;
    jsr multiply  ; call subroutine
    out           ; print AC // or save it before using PLA
                  ; to remove the parameters
    pla           ; removing parameter a from stack
    pla           ; removing parameter b from stack
    pla           ; restoring 'old' AC value
    out           ;

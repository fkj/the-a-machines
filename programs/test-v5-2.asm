;
; demonstrate use of register indirect and register indexed
; 
    lda #5
    pha
    lda #8
    pha
    
    pla         ; move SP back to refer to 8
    lda #3      ; something else than 5 and 8
    
    lda (SP)    ; previous top of stack (i.e. 8)
    out
    lda (SP,#1) ; current top of stack (i.e. 5)
    out
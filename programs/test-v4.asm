;
; simple program for version 4
; 

    ldx #-5     ; X = 5
    cpx #-3     ; compare X with -3 (this will not change X)
    bpl plus    ; if (-5 < -3) goto plus
    lda #0      ; print 0
    out
    jmp end
plus:
    lda #1      ; print 1
    out
end:            ; end of program (notice: no brk)